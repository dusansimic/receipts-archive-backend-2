import Koa from 'koa';
import cors from '@koa/cors';
import bodyParser from 'koa-bodyparser';
import {HttpErrorBase} from '@curveball/http-errors';
import locationsRouter from './routes/locations';
import receiptsRouter from './routes/receipts';
import itemsRouter from './routes/items';

const app = new Koa();
app.use(async (ctx, next) => {
	try {
		await next();
	} catch (error) {
		if (error instanceof HttpErrorBase) {
			ctx.status = error.httpStatus;
			ctx.body = error.message;
		} else if (error instanceof Error) {
			ctx.status = 500;
			ctx.body = error.message;
		}
	}
});
app.use(cors());
app.use(bodyParser());
app.use(locationsRouter.routes());
app.use(locationsRouter.allowedMethods());
app.use(receiptsRouter.routes());
app.use(receiptsRouter.allowedMethods());
app.use(itemsRouter.routes());
app.use(itemsRouter.allowedMethods());

const PORT = process.env.PORT ?? 3000;
app.listen(PORT, () => {
	console.log(`🚀 Running on port ${PORT}`);
});
