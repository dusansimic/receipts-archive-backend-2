import Router from '@koa/router';
import {UnprocessableEntity} from '@curveball/http-errors';
import {
	ReceiptGetObject,
	ReceiptSelectValues,
	ReceiptWhereValues,
	Collector,
	getDateTimeString,
	newPublicId,
	base64,
	connection,
	isTruthy,
	isFalsy
} from '../common';

const router = new Router();

// Get list of receipts
// ! Don't use mutable variables here!
router.get('/receipts', async ctx => {
	// Get the location id of the receipt if the user wants to get receipts from
	// a specific location. Also see if the user wants to get the location data
	// of each receipt returned.
	const {receiptId, locationId, locationData}: {receiptId?: string; locationId?: string; locationData?: string} = ctx.request.query;
	const justOneReceipt = isTruthy(receiptId);
	// If location id is specified, enable search by location
	const searchForLocation = isTruthy(locationId);

	// Add id and created at for selection
	let selectValues: ReceiptSelectValues = {
		id: 'receipts.publicId',
		cost: 'receipts.cost',
		createdAt: 'receipts.createdAt'
	};
	// If location data is requested, add it to the selection
	if (isTruthy(locationData)) {
		selectValues = {
			...selectValues,
			locationId: 'locations.publicId',
			locationName: 'locations.name',
			locationAddress: 'locations.address'
		};
	}

	// If search by location is requested, add location id
	const whereValues: ReceiptWhereValues = {};
	if (justOneReceipt) {
		whereValues['receipts.publicId'] = base64.decode(receiptId as string);
	} else if (searchForLocation) {
		whereValues['locations.publicId'] = base64.decode(locationId as string);
	}

	// Build a query
	const query = connection.select(selectValues).where(whereValues).from('receipts');

	// If either search for location or location data is requested, add join
	if (searchForLocation || isTruthy(locationData)) {
		query.innerJoin('locations', 'locations.id', 'receipts.locationId');
	}

	// Execute the query
	const receipts = await query;

	// Parse all the receipts
	const idParsedReceipts = receipts.map((receipt: ReceiptGetObject): ReceiptGetObject => {
		// Base64 encode the receipt id
		receipt.id = base64.encode(receipt.id as Buffer);
		if (locationData) {
			receipt.location = {
				id: base64.encode(receipt.locationId as Buffer),
				name: receipt.locationName,
				address: receipt.locationAddress
			};

			// Remove props since they are moved to an object
			delete receipt.locationId;
			delete receipt.locationName;
			delete receipt.locationAddress;
		}

		return receipt;
	});

	ctx.body = idParsedReceipts;
	if (justOneReceipt) {
		ctx.body = ctx.body[0];
	}
});

// Create a new receipt
router.post('/receipts', async ctx => {
	// Get receipt data from query
	const {location, datetime: createdAt}: {location?: string; datetime?: string} = ctx.request.body;

	// Check if locationId and createdAt were specified. If not, scream.
	if (isFalsy(location)) {
		throw new UnprocessableEntity('Location id was not specified!');
	}

	if (isFalsy(createdAt)) {
		throw new UnprocessableEntity('Date and time of receipt payment was not specified!');
	}

	// Generate a creation date MariaDB compatable datetime string and add
	// everything to the collector.
	const creationDate = getDateTimeString(new Date(createdAt as string));
	const collector = new Collector();
	collector.add('publicId', newPublicId());

	const locations = await connection.select('id').from('locations').where('publicId', '=', base64.decode(location as string));
	collector.add('locationId', locations[0].id);
	collector.add('createdAt', creationDate);

	// Insert the data
	await connection.insert(collector.toObject()).into('receipts');

	ctx.response.status = 200;
});

// Update receipt info
router.put('/receipts', async ctx => {
	// Get all data from query
	const {id, locationId}: {id?: string; locationId?: string} = ctx.request.body;

	// Check if id and locationId were specified. If not, scream.
	if (isFalsy(id)) {
		throw new UnprocessableEntity('Receipt id is not specified!');
	}

	if (isFalsy(locationId)) {
		throw new UnprocessableEntity('Location id was not specified!');
	}

	// Add data to the collector.
	const collector = new Collector();
	collector.add('locationId', locationId);

	// Update the receipt
	await connection.where('id', '=', base64.decode(id as string)).update(collector.toObject());

	ctx.response.status = 200;
});

// Delete receipt
router.delete('/receipts', async ctx => {
	// Get all data from query
	const {id}: {id?: string} = ctx.request.body;

	// Check if id was specified. If not, scream.
	if (isFalsy(id)) {
		throw new UnprocessableEntity('Receipt id is not specified!');
	}

	await connection('receipts').delete().where('publicId', '=', base64.decode(id as string));

	ctx.response.status = 200;
});

export default router;
