import Router from '@koa/router';
import {UnprocessableEntity} from '@curveball/http-errors';
import {
	Location,
	Collector,
	getDateTimeString,
	newPublicId,
	base64,
	connection,
	isTruthy,
	isFalsy
} from '../common';

const router = new Router();

// Get locations info
router.get('/locations', async ctx => {
	// Get everything form query
	const {name}: {name?: string} = ctx.request.query;

	// Build a query for getting loctions
	const qb = connection.select([{id: 'publicId'}, 'name', 'address', 'createdAt', 'updatedAt']).from('locations');
	// If there is a name, search for it
	if (isTruthy(name)) {
		qb.where('name', 'like', `%${name as string}%`);
	}

	// Execute the query
	const locations = await qb;

	// Base64 encode the binary array for each location
	const idParsedLocations = locations.map((location: Location): Location => {
		location.id = base64.encode(location.id as Buffer);
		return location;
	});

	// Return the final locations list
	ctx.body = idParsedLocations;
});

// Add a location to the database
router.post('/locations', async ctx => {
	// Get the name and address of the location from the request body
	const {name, address}: {name?: string; address?: string} = ctx.request.body;

	// Check if name and address are specified. If not, scream.
	if (isFalsy(name)) {
		throw new UnprocessableEntity('Name of a location is not specified!');
	}

	if (isFalsy(address)) {
		throw new UnprocessableEntity('Address of a location is not specified!');
	}

	// Add everything into a collector
	const creationDate = getDateTimeString(new Date());
	const collector = new Collector();
	collector.add('publicId', newPublicId());
	collector.add('name', name);
	collector.add('address', address);
	collector.add('createdAt', creationDate);
	collector.add('updatedAt', creationDate);

	// Just get the object out of the collector and instert it into the db.
	await connection.insert(collector.toObject()).into('locations');

	ctx.response.status = 200;
});

// Update a location
router.put('/locations', async ctx => {
	// Get the id of the location to update and the data the user wants to update
	const {id, name, address}: {id?: string; name?: string; address?: string} = ctx.request.body;

	if (isFalsy(id)) {
		throw new UnprocessableEntity('Id of location to update is not specified!');
	}

	// Add everything to the collector
	const updateDate = new Date();
	const collector = new Collector();
	collector.add('name', name);
	collector.add('address', address);
	collector.add('updatedAt', getDateTimeString(updateDate));

	// Update the data in the db
	await connection('locations').where('publicId', '=', base64.decode(id as string)).update(collector.toObject());

	ctx.response.status = 200;
});

router.delete('/locations', async ctx => {
	// Get location id from query
	const {id}: {id?: string} = ctx.request.body;

	// Check if id was specified. If not, scream.
	if (isFalsy(id)) {
		throw new UnprocessableEntity('Location id is not specified!');
	}

	await connection('locations').delete().where('publicId', '=', base64.decode(id as string));

	ctx.response.status = 200;
});

export default router;
