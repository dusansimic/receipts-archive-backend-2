import Router from '@koa/router';
import {UnprocessableEntity} from '@curveball/http-errors';
import {
	ItemSelectValues,
	Collector,
	newPublicId,
	base64,
	connection,
	isTruthy,
	isFalsy
} from '../common';

const router = new Router();

// Get list of receipts
router.get('/items', async ctx => {
	// Get all data from query
	const {receiptId, ids}: {receiptId?: string; ids?: string} = ctx.request.query;

	if (isFalsy(receiptId)) {
		throw new UnprocessableEntity('Receipt id was not specified!');
	}

	// If user wants ids, add the id to the beginning of array.
	const columns: ItemSelectValues = {
		name: 'items.name',
		price: 'items.price',
		amount: 'items.amount'
	};
	if (isTruthy(ids)) {
		columns.id = 'items.publicId';
	}

	// Get list of items
	const items = await connection.select(columns).from('items').innerJoin('receipts', 'receipts.id', 'items.receiptId').where('receipts.publicId', '=', base64.decode(receiptId as string));

	// Parse items ids if they have them
	if (isTruthy(ids)) {
		ctx.body = items.map(item => {
			item.id = base64.encode(item.id);
			return item;
		});
	} else {
		ctx.body = items;
	}
});

// Add a new item to a receipt
router.post('/items', async ctx => {
	// Get all data from query
	const {receiptId, name, price, amount}: {receiptId?: string; name?: string; price?: number; amount?: number} = ctx.request.body;

	// Check if required data was specified. If not, scream.
	if (receiptId === undefined) {
		throw new UnprocessableEntity('Receipt id was not specified!');
	}

	const decodedReceiptId = base64.decode(receiptId);
	const receiptPrivateId = await connection.select('id').from('receipts').where('publicId', '=', decodedReceiptId);
	if (receiptPrivateId.length === 0) {
		throw new UnprocessableEntity('Receipt id is not valid!');
	}

	if (name === undefined) {
		throw new UnprocessableEntity('Name of the item was not specified!');
	}

	if (price === undefined) {
		throw new UnprocessableEntity('Price of the item was not specified!');
	}

	// Add all data to the collector
	const collector = new Collector();
	// Private receipt id used to create relations in the database.
	collector.add('receiptId', receiptPrivateId[0].id);
	// Public id used for user access.
	collector.add('publicId', newPublicId());
	collector.add('name', name);
	collector.add('price', price);
	collector.add('amount', amount ?? 1);

	// Insert the new item
	await connection.insert(collector.toObject()).into('items');

	// Increment the total cost of the receipt
	await connection('receipts').where('id', '=', receiptPrivateId[0].id).increment('cost', price * (amount ?? 1));

	ctx.response.status = 200;
});

// Update item
router.put('/items', async ctx => {
	console.time('Total PUT /items');
	// Get all data from query
	const {id, name, price, amount}: {id?: string; name?: string; price?: number; amount?: number} = ctx.request.body;

	// Check if required data was specified. If not, scream.
	if (isFalsy(id)) {
		throw new UnprocessableEntity('Item id was not specified!');
	}

	// Decode the public id
	const decodedPublicId = base64.decode(id as string);

	// Add all data to collector
	const collector = new Collector();
	collector.add('name', name);
	collector.add('price', price);
	collector.add('amount', amount);

	// Update the item and get the updated data.
	await connection('items').where('publicId', '=', decodedPublicId).update(collector.toObject());

	console.time('Get receipt id');
	// Get id of receipt that needs to be updated
	const {receiptId} = (await connection.select('receiptId').from('items').where('publicId', '=', decodedPublicId))[0];
	console.timeEnd('Get receipt id');

	console.time('Get up to date data');
	// Get up to date data of prices and amount
	const upToDateData = await connection.select(['price', 'amount', 'receiptId']).from('items').where('receiptId', '=', receiptId);
	console.timeEnd('Get up to date data');

	console.time('Calculate new cost');
	// Calculate new receipt cost
	const newCost = upToDateData.reduce((acum: number, item: {price: number; amount: number}) => acum + (item.price * item.amount), 0);
	console.timeEnd('Calculate new cost');

	// Update the cost in receipt.
	await connection('receipts').where('id', '=', receiptId).update({cost: newCost});

	ctx.response.status = 200;
	console.timeEnd('Total PUT /items');
});

router.delete('/items', async ctx => {
	// Get item id from query
	const {id}: {id?: string} = ctx.request.body;

	// Check if id was specified. If not, scream.
	if (isFalsy(id)) {
		throw new UnprocessableEntity('Item id is not specified!');
	}

	await connection('items').delete().where('publicId', '=', base64.decode(id as string));

	ctx.response.status = 200;
});

export default router;
