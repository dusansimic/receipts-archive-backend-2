import { Config } from 'knex';

const config: Config = {
	client: process.env.SERVER_CLIENT || 'mysql',
	connection: {
		host: process.env.SERVER_HOST || 'localhost',
		user: process.env.SERVER_USER || 'dusan',
		password: process.env.SERVER_PASSWORD || 'dusanpass',
		database: process.env.SERVER_DATABASE || 'receipts'
	},
	pool: {
		min: 0,
		max: 10
	}
};

export default config;