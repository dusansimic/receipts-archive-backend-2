import crypto from 'crypto';
import Knex from 'knex';
import baseX from 'base-x';
import config from './config';

export interface Location {
	id: Buffer | string;
	name: string;
	address: string;
	createdAt: Date;
	updatedAt: Date;
}

export interface LocationForGetObject {
	id: string;
	name: string;
	address: string;
}

export interface Receipt {
	pk: number;
	id: Buffer | string;
	location: string | LocationForGetObject;
	locationId: Buffer | string;
	cost: number;
	createdAt: Date;
}

export interface ReceiptGetObject extends Receipt {
	location: LocationForGetObject;
	locationName: string;
	locationAddress: string;
}

export interface Item {
	id: Buffer;
	receiptId: Buffer;
	name: string;
	price: number;
	amount: number;
}

export interface ItemSelectValues {
	id?: string;
	name: string;
	price: string;
	amount: string;
}

export interface ReceiptSelectValues {
	id: string;
	cost: string;
	createdAt: string;
	locationId?: string;
	locationName?: string;
	locationAddress?: string;
}

export interface ReceiptWhereValues {
	'receipts.publicId'?: Buffer;
	'locations.publicId'?: Buffer;
}

export class Collector {
	toObject(): object {
		return {...this};
	}

	// eslint-disable-next-line @typescript-eslint/no-untyped-public-signature
	add(this: any, key: string, val: any): boolean {
		if (val === null || val === undefined) {
			return false;
		}

		this[key] = val;
		return true;
	}
}

// Create a base64 encoder (not base64 real base64 but the one that converts
// a number into 64 base number)
// Every mention of base64 encoding in this file is refeering to this encoder
// unless specified otherwise
const Base64Alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_';
export const base64 = baseX(Base64Alphabet);

// Use instead of uuid because it's faster and I just need a simple id to show
// on the web app
export const newPublicId = (): Buffer => crypto.randomBytes(16);
// Get a MariaDB compatable date time string from the date object in JS
export const getDateTimeString = (date: Date): string => date.toISOString().replace('T', ' ').replace('Z', '');
// Convert string with boolean value to boolean
export const getBoolean = (bool: any): boolean => typeof bool === 'string' && bool.toLowerCase() === 'true';

export const isTruthy = (val: any): boolean => val !== undefined && val !== null && val !== '' && val !== 0 && val !== false && (typeof val === 'string' && val.toLowerCase() !== 'false');
export const isFalsy = (val: any): boolean => !isTruthy(val);

export const connection: Knex = Knex(config); // eslint-disable-line new-cap

